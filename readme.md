# catch-of-the-day

> A real-time app for a trendy seafood market where price and quantity available are variable and can change at a moment's notice. With a menu, an order form, and an inventory management area where authorized users can immediately update product details.
> To see the application running, go to:
http://react-catch-of-the-day.surge.sh/

## Course
> So far, the course i did were:
- https://reactforbeginners.com/

## How to Run

1. Run git clone
  - ssh: git clone `git@github.com:lucasmls/react-catch-of-the-day.git`
  - https: git clone `https://github.com/lucasmls/react-catch-of-the-day.git`
2. Install the dependencies with `npm install`.
3. Run the application with `npm start`.
