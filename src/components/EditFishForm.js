import React, { Component } from 'react'
import PropTypes from 'prop-types'

class EditFishForm extends Component {

  static propTypes = {
    fish: PropTypes.shape({
      name: PropTypes.string,
      image: PropTypes.string,
      desc: PropTypes.string,
      status: PropTypes.string,
      price: PropTypes.number,
    }),
    index: PropTypes.string,
    updatedFish: PropTypes.func
  }

  handleChange = (event) => {
    const { name: inputName, value: inputValue } = event.currentTarget
    const updatedFish = {
      ...this.props.fish,
      [inputName]: inputValue
    }
    this.props.updateFish(this.props.index, updatedFish)
  }

  render() { 
    return (
      <div className="fish-edit">
        <input 
          name="name"
          type="text"
          placeholder="Name"
          value={this.props.fish.name}
          onChange={this.handleChange} />
        <input 
          name="price"
          type="text"
          placeholder="Price"
          value={this.props.fish.price}
          onChange={this.handleChange} />

        <select
          name="status"
          value={this.props.fish.status}
          onChange={this.handleChange}>
            <option value="available">Fresh!</option>
            <option value="unavailable">Sold Out!</option>
        </select>

        <textarea
          name="desc"
          placeholder="Description"
          value={this.props.fish.desc}
          onChange={this.handleChange} />

        <input
          name="image"
          type="text"
          placeholder="Image"
          value={this.props.fish.image}
          onChange={this.handleChange} />

        <button
          onClick={() => this.props.deleteFish(this.props.index)}>
          Remove Fish
        </button>
      </div>
    )
  }
}

export default EditFishForm