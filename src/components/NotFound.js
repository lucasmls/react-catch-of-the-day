import React from 'react'

const style = {
  paddingTop: '50px'
}

const NotFound = () => (
  <div style={style}>
    <h2>404! Not found!?!?!</h2>
  </div>
)

export default NotFound